CC = gcc
LIBS = -lncurses
CFLAGS = -Wall

IDIR = include
CDIR = src
ODIR = $(CDIR)/obj

_DEPS = snake.h
DEPS = $(patsubst %,$(IDIR)/%,$(_DEPS))

_OBJ = snake.o main.o
OBJ = $(patsubst %,$(ODIR)/%,$(_OBJ))

#$(ODIR)/%.o: $(CDIR)/%.c $(DEPS)
#	$(CC) -c -o $@ $< $(CFLAGS)

$(ODIR)/%.o: $(CDIR)/%.c $(_DEPS)
	$(CC) -c -o $@ $< $(CFLAGS) -I$(IDIR)

main: $(OBJ)
	$(CC) -o snake $^ $(CFLAGS) $(LIBS)

.PHONY: clean

clean:
	rm ./snake
install:
	sudo cp ./snake /usr/bin/snake

uninstall:
	sudo rm /usr/bin/snake
