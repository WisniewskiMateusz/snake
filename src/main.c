#include <time.h>
#include "snake.h"

int minx, miny, maxx, maxy;
int itemx, itemy;
bool item_collected;

int main() 
{
	int c = 0;
	int dir;
	int snake_len = 5;
	SNAKE *snake = NULL;
	time_t start;

	srand(time(NULL));
	
	init_window();

	do {
		clear_screen();

		dir = RIGHT;
		snake = init_snake(minx + snake_len * 2 + 1, miny + 5, GREEN, snake_len, dir);

		start = time(NULL);

		item_collected = true;
	
		while(true){
			c = getch();
			if	(c == KEY_RIGHT)
				dir = RIGHT;
			else if	(c == KEY_LEFT)
				dir = LEFT;
			else if	(c == KEY_UP)
				dir = UP;
			else if	(c == KEY_DOWN)
				dir = DOWN;
			else if	(c == 'q')
				return 0;
		
			if(move_snake(snake, dir) == false)
				break;
	
			if(item_collected) {
				print_item(snake);
				item_collected = false;
			}
	
			update_score(snake);
			update_time(time(NULL) - start);
			refresh();
		}

		delete_snake(snake);
		print_lose();

		while(true){
			c = getch();

			if	(c == 'y')
				break;
			else if	(c == 'n' || c == 'q')
				return 0;
		}
	} while(true);

	return 0;
}
