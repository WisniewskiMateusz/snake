#include "snake.h"

void init_window() 
{
	extern int minx, miny, maxx, maxy;
	int border_color = BLUE;

	initscr();						// screen initialization
	atexit(finish);						// register finish function to be executed on exiting the program
	getmaxyx(stdscr, maxy, maxx);				// get console size (getmaxyx is a macro, so it does not take pointers as arguments)
	miny = 5;
	minx = 2;
	maxy -= 2;
	maxx -= 3;

	noecho();						// disable echoing user input
	halfdelay(1);						// setting a delay for getch
	keypad(stdscr, true);					// enable special keys
	curs_set(false);					// disable cursor
	start_color();						// start using colors
	use_default_colors();					// use default console colors

	if(can_change_color()){					// initialize custom colors
		init_color(COLOR_BLACK, 0, 0, 0);
		init_color(COLOR_RED, 1000, 0, 0);
		init_color(COLOR_GREEN, 0, 1000, 0);
		init_color(COLOR_YELLOW, 1000, 1000, 0);
		init_color(COLOR_BLUE, 0, 0, 1000);
		init_color(COLOR_WHITE, 1000, 1000, 1000);
		init_color(COLOR_MAGENTA, 1000, 0, 564);
	}

	init_pair(FG, COLOR_WHITE, -1);					// init fg, bg color pairs
	init_pair(BLACK, COLOR_WHITE, COLOR_BLACK);
	init_pair(RED, COLOR_BLACK, COLOR_RED);
	init_pair(GREEN, COLOR_BLACK, COLOR_GREEN);
	init_pair(YELLOW, COLOR_BLACK, COLOR_YELLOW);
	init_pair(BLUE, COLOR_BLACK, COLOR_BLUE);
	init_pair(MAGENTA, COLOR_BLACK, COLOR_MAGENTA);
	init_pair(CYAN, COLOR_BLACK, COLOR_CYAN);
	init_pair(WHITE, COLOR_BLACK, COLOR_WHITE);

	if((maxx - minx) % 2 == 0)					// each segment's length is 2
		maxx -= 1;

	attron(COLOR_PAIR(border_color));

	for(int i = 0; i < maxx + 3; i++){				// print border
		mvaddch(0, i, ' ');
		mvaddch(miny - 1, i, ' ');
		mvaddch(maxy + 1, i, ' ');
	}
	
	for(int i = 0; i < maxy + 1; i++) {
		mvaddstr(i, 0, "  ");
		mvaddstr(i, maxx + 1, "  ");
	}

	attroff(COLOR_PAIR(border_color));

	attron(COLOR_PAIR(FG));
	mvprintw((miny - 1)/2, minx + 10, "Score: ");
	mvprintw((miny - 1)/2, maxx - 20, "Time: ");
	attroff(COLOR_PAIR(FG));
}

void clear_screen()
{
	extern int minx, miny, maxx, maxy;
	
	for(int i = miny; i <= maxy; i++)
		for(int j = minx; j <= maxx; j++)
			mvaddch(i, j, ' ' | COLOR_PAIR(FG));
}

void print_lose()
{
	extern int minx, miny, maxx, maxy;

	clear_screen();

	mvaddstr((maxy - miny)/2 - 3, (maxx - minx)/2 - 10, "You lost. Try again? (y, n)");
}

SNAKE* init_snake(int posx, int posy, int color, int len, int dir) 
{
	SEG *temp;
	
	SNAKE *snake 		= malloc(sizeof(SNAKE));	// allocate memory for the snake

	snake->score		= 0;
	snake->dir 		= dir;

	snake->last 		= malloc(sizeof(SEG));		// allocate memory for the last segment
	snake->last->posx 	= posx - 2*len + 1;
	snake->last->posy 	= posy;
	snake->last->color 	= color;
	
	temp			= snake->last;
	for(int i = 2 * len - 3; i >= 0; i -= 2){			// allocate memory for the rest of segments
		temp->next		= malloc(sizeof(SEG));
		temp			= temp->next;
		temp->posx		= posx - i;
		temp->posy		= posy;
		temp->color		= color;
		temp->next 		= NULL;
	}
	snake->first		= temp;
	
	if(color == MAGENTA)
		snake->first->color	= RED;
	else
		snake->first->color	= MAGENTA;

	return snake;
}

void delete_snake(SNAKE *snake)
{
	SEG *tempnext = NULL;
	for(SEG *temp = snake->last; temp != NULL; temp = tempnext){
		tempnext = temp->next;
		free(temp);
	}

	free(snake);
}

bool move_snake(SNAKE *snake, int dir) 
{
	extern int minx, miny, maxx, maxy;
	extern int itemx, itemy;
	extern bool item_collected;
	int newx = snake->first->posx, newy = snake->first->posy;

	if(!(	((snake->dir == UP) && (dir == DOWN)) || \
		((snake->dir == DOWN) && (dir == UP)) || \
		((snake->dir == LEFT) && (dir == RIGHT)) || \
		((snake->dir == RIGHT) && (dir == LEFT))))
			snake->dir = dir;

	if(snake->dir == UP)
		newy -= 1;
	else if(snake->dir == DOWN)
		newy += 1;
	else if(snake->dir == LEFT)
		newx -= 2;
	else if(snake->dir == RIGHT)
		newx += 2;
		
	if(newx < minx || newx > maxx || newy < miny || newy > maxy)
		return false;

	if(is_in_snake(snake, newx, newy))
		return false;

	print_snake(snake, true);

	if(newx == itemx && newy == itemy){
		item_collected = true;
		add_segment(snake);
	}

	for(SEG *temp = snake->last; temp->next != NULL; temp = temp->next){
		temp->posx = temp->next->posx;
		temp->posy = temp->next->posy;
	}

	snake->first->posx = newx;
	snake->first->posy = newy;

	print_snake(snake, false);

	return true;
}

void print_snake(SNAKE *snake, bool clear) 
{
	int color;

	for(SEG *temp = snake->last; temp != NULL; temp = temp->next){
		if(clear == true)
			color = FG;
		else
			color = temp->color;

		attron(COLOR_PAIR(color));
		mvaddstr(temp->posy, temp->posx, "  ");
	}
}

void add_segment(SNAKE *snake)
{
	SEG *new_seg 	= malloc(sizeof(SEG));

	new_seg->color 	= snake->last->color;
	new_seg->next 	= snake->last;
	snake->last 	= new_seg;

	snake->score	+= 1;
}

bool is_in_snake(SNAKE *snake, int x, int y)
{
	for(SEG *temp = snake->last; temp->next != NULL; temp = temp->next)
		if(temp->posx == x && temp->posy == y)
			return true;
	return false;
}

bool is_in_head(SNAKE *snake, int x, int y)
{
	if(snake->first->posx == x && snake->first->posy == y)
		return true;
	else
		return false;
}

void print_item(SNAKE *snake)
{
	extern int minx, miny, maxx, maxy;
	extern int itemx, itemy;

	do {
		itemx = (int)(((float)rand() / RAND_MAX) * (maxx - minx) + minx);
		if((itemx - minx) % 2 == 1)
			itemx -= 1;
		itemy = (int)(((float)rand() / RAND_MAX) * (maxy - miny) + miny);
	} while(is_in_snake(snake, itemx, itemy) || is_in_head(snake, itemx, itemy));
	
	attron(COLOR_PAIR(YELLOW));
	mvaddstr(itemy, itemx, "  ");
	attroff(COLOR_PAIR(YELLOW));
}

void update_time(time_t time)
{
	extern int maxx, miny;
	int hours = time/3600, minutes = (time - 3600*hours)/60, seconds = time - 3600*hours - 60*minutes;
	attron(COLOR_PAIR(FG));
	mvprintw((miny - 1)/2, maxx - 14, "%02d:%02d:%02d", hours, minutes, seconds);
	attroff(COLOR_PAIR(FG));
}

void update_score(SNAKE *snake)
{
	extern int miny, minx;

	attron(COLOR_PAIR(FG));
	mvprintw((miny - 1)/2, minx + 17, "%d    ", snake->score);
	attroff(COLOR_PAIR(FG));
}

void finish(void) 
{
	endwin();
}
