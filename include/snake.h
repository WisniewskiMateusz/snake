#include <stdlib.h>
#include <ncurses.h>

#define BLACK 	1
#define RED 	2
#define GREEN 	3
#define YELLOW 	4
#define BLUE 	5
#define MAGENTA	6
#define CYAN 	7
#define WHITE 	8
#define FG	9

#define UP	0
#define DOWN	1
#define LEFT	2
#define RIGHT	3

/* Structure representing one snake's segment. */
typedef struct _SEG 
{
	struct _SEG *next;
	int posx;
	int posy;
	int color;
} SEG;

/* Structure representing whole snake. */
typedef struct _SNAKE 
{
	SEG *first;
	SEG *last;
	int score;
	int dir;
} SNAKE;

/* Function initializing console and whole program. */
void init_window();

/* Function clearing the screen. */
void clear_screen();

/* Function printing screen when the player loses. */
void print_lose();

/* Function returning an initialized snake. */
SNAKE* init_snake(int posx, int posy, int color, int len, int dir);

/* Function deleting snake and freeing memory. */
void delete_snake(SNAKE *snake);

/* Function responsible for moving the snake for one unit. 
 * Returns 'true' on success, 'false' when the snake hits a border or himself. */
bool move_snake(SNAKE *snake, int dir);

/* Function printing or clearing the snake. */
void print_snake(SNAKE *snake, bool clear);

/* Function adding a segment to snake's tail. */
void add_segment(SNAKE *snake);

/* Function telling if a point is inside snake (without head). */
bool is_in_snake(SNAKE *snake, int x, int y);

/* Function telling if a point is inside snake's head. */
bool is_in_head(SNAKE *snake, int x, int y);

/* Function printing an item to collect by a snake. */
void print_item(SNAKE *snake);

/* Function updating current time. */
void update_time(time_t time);

/* Function updating current score. */
void update_score(SNAKE *snake);

/* Function to be executed on exiting the program. */
void finish(void);
